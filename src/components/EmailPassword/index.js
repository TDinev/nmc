import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import "./styles.scss";
import Button from "../forms/Button";
import FormInput from "../forms/FormInput";
import AuthWrapper from "../AuthWrapper";
import { useDispatch, useSelector } from "react-redux";
import {
	resetPasswordStart,
	resetUserState,
} from "../../redux/User/user.actions";

const mapState = ({ user }) => ({
	resetPasswordSuccess: user.resetPasswordSuccess,
	userErr: user.userErr,
});

const EmailPassword = (props) => {
	const history = useHistory();
	const { resetPasswordSuccess, userErr } = useSelector(mapState);
	const dispatch = useDispatch();
	const [email, setEmail] = useState("");
	const [errors, setErrors] = useState([]);

	useEffect(() => {
		if (resetPasswordSuccess) {
			dispatch(resetUserState());
			history.push("/login");
		}
	}, [dispatch, history, resetPasswordSuccess]);

	useEffect(() => {
		if (Array.isArray(userErr) && userErr.length > 0) {
			setErrors(userErr);
		}
	}, [userErr]);

	const handleSubmit = (e) => {
		e.preventDefault();
		dispatch(resetPasswordStart({ email }));
	};

	const configAuthWrapper = {
		headline: "Email Password",
	};
	return (
		<AuthWrapper {...configAuthWrapper}>
			<div className="formWrap">
				{errors.length > 0 && (
					<ul>
						{errors.map((err, index) => {
							return <li key={index}>{err}</li>;
						})}
					</ul>
				)}
				<form onSubmit={handleSubmit}>
					<FormInput
						type="email"
						name="email"
						value={email}
						placeholder="Email"
						handleChange={(e) => setEmail(e.target.value)} //In FormInput index.js onChange={handleChange} and otherProps
					/>
					<Button type="submit">Send email</Button>
				</form>
			</div>
		</AuthWrapper>
	);
};

export default EmailPassword;
