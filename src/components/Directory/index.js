import React from 'react';
import chip from '../../assets/chip.jpg';
import appliances from '../../assets/appliances.PNG';
import './styles.scss'

const Directory = props => {
    return (
        <div className='directory'>
            <div className='wrap'>
                <div
                    className='item'
                    style={{
                        backgroundImage: `url(${chip})`
                    }}>
                    <a>
                        Shop Chip
                    </a>
                </div>
                <div
                    className='item'
                    style={{
                        backgroundImage: `url(${appliances})`
                    }}>
                    <a>
                        Shop Appliance
                    </a>
                </div>
            </div>
            
        </div>
    );
};

export default Directory;