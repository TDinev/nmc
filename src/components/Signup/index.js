import React, { useState, useEffect } from "react";
import "./styles.scss";
import FormInput from "../../components/forms/FormInput";
import Button from "../../components/forms/Button";
import AuthWrapper from "../AuthWrapper";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { signUpUserStart } from "../../redux/User/user.actions";

const mapState = ({ user }) => ({
	currentUser: user.currentUser,
	userErr: user.userErr,
});

const Signup = (props) => {
	const history = useHistory();
	const dispatch = useDispatch();
	const { currentUser, userErr } = useSelector(mapState);
	const [fullName, setFullName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [errors, setErrors] = useState([]);

	useEffect(() => {
		if (currentUser) {
			resetForm();
			history.push("/");
		}
	}, [dispatch, history, currentUser]);

	useEffect(() => {
		if (Array.isArray(userErr) && userErr.length > 0) {
			setErrors(userErr);
		}
	}, [userErr]);

	const resetForm = () => {
		setFullName("");
		setEmail("");
		setPassword("");
		setConfirmPassword("");
	};

	const handleFormSubmit = (e) => {
		e.preventDefault();
		dispatch(
			signUpUserStart({
				fullName,
				email,
				password,
				confirmPassword,
			})
		);
	};

	const configAuthWrapper = {
		headline: "Registration",
	};

	return (
		<AuthWrapper {...configAuthWrapper}>
			<div className="formWrap">
				{errors.length > 0 && (
					<ul>
						{errors.map((err, index) => {
							return <li key={index}>{err}</li>;
						})}
					</ul>
				)}
				<form onSubmit={handleFormSubmit}>
					<FormInput
						type="text"
						name="fullName"
						value={fullName}
						placeholder="Full Name"
						handleChange={(e) => setFullName(e.target.value)} //In FormInput index.js onChange={handleChange} and otherProps
					/>
					<FormInput
						type="email"
						name="email"
						value={email}
						placeholder="Email"
						handleChange={(e) => setEmail(e.target.value)}
					/>
					<FormInput
						type="password"
						name="password"
						value={password}
						placeholder="Password"
						handleChange={(e) => setPassword(e.target.value)}
					/>
					<FormInput
						type="password"
						name="confirmPassword"
						value={confirmPassword}
						placeholder="Confirm Password"
						handleChange={(e) => setConfirmPassword(e.target.value)}
					/>
					<Button type="submit">Register</Button>
				</form>
			</div>
		</AuthWrapper>
	);
};

export default Signup;
